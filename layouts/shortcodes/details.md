<details open="true" id="{{ if .Get "id" }}{{ .Get "id" }}{{ else }}{{ .Get "title" }}{{ end }}">
  <summary class="h4">{{ with .Get "href" }}<a href="{{ . }}">{{ end }}{{ .Get "title" }}{{ with .Get "href" }}</a>{{ end }}</summary>
  {{- .Inner | markdownify -}}
</details>
